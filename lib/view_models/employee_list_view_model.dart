import 'package:flutter/material.dart';
import 'package:xaltius_snapshot/services/webservice.dart';
import 'package:xaltius_snapshot/view_models/employee_view_model.dart';

class EmployeeListViewModel extends ChangeNotifier {
  List<EmployeeViewModel> employees = List<EmployeeViewModel>();

  Future<void> fetchEmployees(String baseUrl, String port) async {
    final results = await Webservice().fetchEmployee(baseUrl, port);

    this.employees = results.map((e) => EmployeeViewModel(employee: e)).toList();
    print(this.employees);
    notifyListeners();
  }
}