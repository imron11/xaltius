import 'package:xaltius_snapshot/models/employee.dart';

class EmployeeViewModel {
  final Employee employee;

  EmployeeViewModel({this.employee});

  String get name {
    return this.employee.name;
  }

  String get role {
    return this.employee.role;
  }
}