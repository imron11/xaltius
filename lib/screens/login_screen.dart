import 'package:flutter/material.dart';
import 'package:xaltius_snapshot/constants.dart';
import 'package:xaltius_snapshot/screens/home_screen.dart';

class LoginScreen extends StatefulWidget {
  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {

  String ip = "";
  int port = 0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        width: MediaQuery.of(context).size.width,
        color: kBackgroundColor,
        child: Padding(
          padding: const EdgeInsets.all(40.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Text(
                "XALTIUS",
                style: kLargeTitleStyle.copyWith(fontSize: 48).apply(color: kSecondaryLabelColor),
              ),
              Text(
                "snapshot",
                style: kLargeTitleStyle.apply(fontStyle: FontStyle.italic, color: kSecondaryLabelColor),
              ),
              SizedBox(
                height: 30,
              ),
              Container(
                height: 130,
                child: Column(
                  children: [
                    Container(
                      decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(14),
                          boxShadow: [
                            BoxShadow(
                                color: kShadowColor,
                                offset: Offset(0, 12),
                                blurRadius: 16
                            )
                          ]
                      ),
                      child: Column(
                        children: [
                          Padding(
                            padding: EdgeInsets.only(
                              top: 5,
                              right: 24,
                              left: 24,
                            ),
                            child: TextField(
                              cursorColor: kPrimaryLabelColor,
                              decoration: InputDecoration(
                                  border: InputBorder.none,
                                  hintText: "IP ADDRESS OR URL",
                                  hintStyle: kLoginInputTextStyle
                              ),
                              style: kLoginInputTextStyle.copyWith(color: Colors.black),
                              onChanged: (value) {
                                ip = value;
                              },
                            ),
                          ),
                          Divider(),
                          Padding(
                            padding: EdgeInsets.only(
                              bottom: 5,
                              right: 24,
                              left: 24,
                            ),
                            child: TextField(
                              cursorColor: kPrimaryLabelColor,
                              keyboardType: TextInputType.number,
                              obscureText: true,
                              decoration: InputDecoration(
                                  border: InputBorder.none,
                                  hintText: "PORT",
                                  hintStyle: kLoginInputTextStyle
                              ),
                              style: kLoginInputTextStyle.copyWith(color: Colors.black),
                              onChanged: (value) {
                                port = int.parse(value);
                              },
                            ),
                          ),

                        ],
                      ),
                    )
                  ],
                ),
              ),
              SizedBox(
                height: 30,
              ),
              GestureDetector(
                onTap: () {
                  // Navigator.push(
                  //   context,
                  //   MaterialPageRoute(
                  //       builder: (context) => HomeScreen(),
                  //       fullscreenDialog: true
                  //   ),
                  // );
                },
                child: Container(
                  height: 50,
                  alignment: Alignment.center,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(14),
                      color: Color(0xff73a0f4)
                  ),
                  child: Text(
                    "Submit",
                    style: kCalloutLabelStyle.copyWith(color: Colors.white),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
