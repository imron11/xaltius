import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:xaltius_snapshot/constants.dart';

class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height,
        color: kBackgroundColor,
        child: Stack(
          children: [
            SafeArea(
              child: Padding(
                padding: EdgeInsets.all(40),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text("List of Employee",
                        style: kLargeTitleStyle.copyWith(
                            fontSize: 32, fontStyle: FontStyle.italic)),
                    SizedBox(height: 12),
                    SingleChildScrollView(
                      child: Padding(
                        padding: const EdgeInsets.only(top: 10.0),
                        child: Column(
                          children: [
                            Container(
                              height: 60,
                              child: Row(
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: [
                                  Text(
                                    "Imron".toUpperCase(),
                                    style: kLargeTitleStyle.apply(
                                        color: kSecondaryLabelColor),
                                  ),
                                  Spacer(),
                                  Text(
                                    "Developer".toUpperCase(),
                                    style: kTitle1Style.apply(
                                        color: Color(0x721b1e9c),
                                        fontStyle: FontStyle.italic).copyWith(fontSize: 16),
                                  ),
                                ],
                              ),
                            ),
                            Divider(color: Colors.black)
                          ],
                        ),
                      ),
                    )
                  ],
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
