class Employee {
  final String name;
  final String role;

  Employee({this.name, this.role});

  factory Employee.fromJson(Map<String, dynamic> json) {
    return Employee(
      name: json["name"],
      role: json["role"]
    );
  }
}