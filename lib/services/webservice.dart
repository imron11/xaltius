import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:xaltius_snapshot/models/employee.dart'; 

class Webservice {
  Future<List<Employee>> fetchEmployee(String baseUrl, String port) async {
    var url = "http://$baseUrl/data";

    if (port != "") {
      url = "http://$baseUrl:$port/data";
    } 

    final response = await http.get(new Uri.http(url, "/data"));
    if(response.statusCode == 200) {

       final body = jsonDecode(response.body); 
       final Iterable json = body["Search"];
       return json.map((res) => Employee.fromJson(res)).toList();

    } else {
      throw Exception("Unable to perform request!");
    }
  }
}